FROM quay.io/jupyter/datascience-notebook:2024-03-14
RUN mamba install -y -c conda-forge bash jupyter jupyter_contrib_nbextensions
RUN mamba install -y -c conda-forge xeus-cling xtensor

# install openCV
USER root
RUN apt-get update && apt-get install -y cmake g++ wget unzip

USER jovyan
RUN mkdir -p /home/jovyan/work/opencv
WORKDIR /home/jovyan/work/opencv
RUN wget -O opencv.zip https://github.com/opencv/opencv/archive/4.9.0.zip
RUN unzip opencv.zip
RUN mkdir -p build
WORKDIR /home/jovyan/work/opencv/build
RUN cmake ../opencv-4.9.0
RUN cmake --build .

USER root
RUN cmake --install .

USER jovyan
WORKDIR /home/jovyan/work

